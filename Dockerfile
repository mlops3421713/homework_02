# Development target
FROM continuumio/miniconda3 as dev

WORKDIR /app

# STUB: Copying files
COPY test.py .

# Creating dev environment
COPY dev.yaml .
RUN conda env create -f dev.yaml

# Copying project settings
COPY .pre-commit-config.yaml .
COPY pyproject.toml .

# STUB: run the development environment
ENTRYPOINT ["conda", "run", "-n", "dev", "python", "test.py"]


# Deployment target
FROM continuumio/miniconda3

WORKDIR /app

# Copy necessary files built by dev stage
COPY --from=dev /app/test.py .

# Creating prod environment
COPY prod.yaml .
RUN conda env create -f prod.yaml

# STUB: run the application
ENTRYPOINT ["conda", "run", "-n", "prod", "python", "test.py"]
