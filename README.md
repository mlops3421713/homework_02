# MLops homework: Docker

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

## Building and running the docker image

### Development build (brings in pre-commit, ruff, and mypy in addition to runtime dependencies)

docker build . --target dev -t <your_image_name>
docker run <your_image_name>


### Deployment build (brings in actual dependencies only)

docker build .  -t <your_image_name>
docker run <your_image_name>

## Runtime dependencies

- celery





